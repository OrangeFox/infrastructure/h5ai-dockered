# Copyright (C) 2018-2020 OrangeFox Recovery Project
# This file is part of the OrangeFox Recovery Project.

FROM nginx
MAINTAINER MrYacha

EXPOSE 8085

ADD moved.html /var/www/html/moved.html

COPY nginx_h5ai.conf /etc/nginx/nginx.conf

CMD ["nginx", "-g", "daemon off;"]